var lives = 0;
var coins = 5;
var endedAnimations = 3;

var animEndEventNames = {
    'WebkitAnimation' : 'webkitAnimationEnd',
    'OAnimation' : 'oAnimationEnd',
    'msAnimation' : 'MSAnimationEnd',
    'animation' : 'animationend'
}

var animationEndName = animEndEventNames[Modernizr.prefixed('animation')];

Mediator.subscribe('inputgiven', function() {
	$.ajax({
		url: "http://dragonquest.at/numbers.php?jsonp",
		dataType: "jsonp",
		jsonpCallback: "callback",
		success: function(data) {
			Mediator.publish('datareceived', data);
		},
		error: function(err) {
			alert("Fehler");
		}
	});
});

Mediator.subscribe('endOfAnimation', function() {
	if(coins === 0) {
    	Mediator.publish('playGameOverSound');
	}
	else {
		if($('.coins').text() == coins) {
			Mediator.publish('playLoseSound');
		}
		else {
			Mediator.publish('playWinSound');
		}
	}
    Mediator.publish('updateCoins');
});

Mediator.subscribe('startOfAnimation', function() {
    Mediator.publish('playStartSound');
    Mediator.publish('updateCoins');
});

var firstUpdate = true;

var updateCoins = function(){
	$('.coins').text(coins);
};
updateCoins();

firstUpdate = false;

Mediator.subscribe('updateCoins', updateCoins);

var updateLives = function(){
	$('.lives').text(lives);
};
updateLives();

Mediator.subscribe('updateLives', updateLives);

Mediator.subscribe('inputgiven', function() {
	coins = coins - 1;
	Mediator.publish('startOfAnimation');
});

Mediator.subscribe('datareceived', function(data) {
	Mediator.publish('refreshSlots', data[0].slots);
	if(data[0].result != 0) {
		coins = coins + data[0].result;
	}
});

var refreshClasses = function(classname, newclass) {
	var div = $('.' + classname);
	requestAnimationFrame(function() {
		div.removeClass();
		requestAnimationFrame(function() {
			div.addClass(classname + " slot-picture " + newclass);
		})
	});
};

Mediator.subscribe('refreshSlots', function(slots) {
	var image1 = slots[0][0] + 1;
	var image2 = slots[0][1] + 1;
	var image3 = slots[0][2] + 1;
	var duration1 = Math.floor((Math.random()*10)+1);
	var duration2 = Math.floor((Math.random()*10)+1);
	var duration3 = Math.floor((Math.random()*10)+1);
	var random1 = Math.floor((Math.random()*4)+1);
	var random2 = Math.floor((Math.random()*4)+1);
	var random3 = Math.floor((Math.random()*4)+1);
	var class1 = "image" + image1 + "-duration" + duration1 + "-randomized" + random1;
	var class2 = "image" + image2 + "-duration" + duration2 + "-randomized" + random2;
	var class3 = "image" + image3 + "-duration" + duration3 + "-randomized" + random3;
	refreshClasses('slot1', class1);
	refreshClasses('slot2', class2);
	refreshClasses('slot3', class3);
});

$('.slot-picture').each(function() {
	$(this).on(animationEndName, function() {
		endedAnimations = endedAnimations + 1;
		if(endedAnimations == 3) {
			Mediator.publish('endOfAnimation');
		}
	})
});

$('html').on('click', function () {
	if(endedAnimations >= 3) {
		if(coins > 0) {
    		Mediator.publish('inputgiven');
			endedAnimations = 0;
    	}
    	else {
    		alert("You ran out of coins, please refresh the page...");
    	}
    }
});

$('.slot1').addClass('image' + Math.floor((Math.random()*4)+1));
$('.slot2').addClass('image' + Math.floor((Math.random()*4)+1));
$('.slot3').addClass('image' + Math.floor((Math.random()*4)+1));