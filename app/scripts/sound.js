var startsound = new Audio("");
var gameoversound = new Audio("");
var winsound = new Audio("");
var losesound = new Audio("");

var body = $('body');
body.append(startsound);
body.append(gameoversound);
body.append(winsound);
body.append(losesound);

winsound.src = "assets/smb2_bonus_chance_win.wav"
losesound.src = "assets/smb2_bonus_chance_lose.wav"
startsound.src = "assets/smb2_bonus_chance_start.wav"
gameoversound.src = "assets/smb2_game_over.wav"

Mediator.subscribe('playStartSound', function() {
	startsound.play();
});

Mediator.subscribe('playGameOverSound', function() {
	gameoversound.play();
});

Mediator.subscribe('playWinSound', function() {
	winsound.play();
});

Mediator.subscribe('playLoseSound', function() {
	losesound.play();
});